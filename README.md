# README #

This Repo contains definitions of the Middle-Office Mock API
The Mock Server sources are on github [json-mock-hapi](https://github.com/eferte/json-mock-hapi)

### Requirements ###

* install node js,
* clone the project,
* type the command:

        npm install
     
  (this will install other required libraries.)

### Start the server ###

    node start 
    
 This has the effect to start the mock using port 3000 and the folder "./api"
 for services configuration.
    
  - start the server by specifying the port:

        node start -p 3000

  - start the server by specifying the services configuration folder:       

        node start -f './services-definition'

  - start the server and listen changes in the configuration folder:

        node start -w

  - list all available arguments:

        node start --help

  - complete sample:
  
        node start -p 3000 -f './api' -w
            
### using the server ###

try :

	GET    /back/catalog/content
	GET    /back/catalog/content/{id}
	GET    /back/catalog/{objectType}/{id}/contents
	GET    /back/catalog/{objectType}/{id}/diff
	GET    /back/catalog/{objectType}/{id}/filter
	GET    /back/catalog/{objectType}/{id}/member
	GET    /back/catalog/{objectType}/{id}/member/{function}
	GET    /back/catalog/{objectType}/{id}/related
	GET    /back/catalog/{objectType}/{id}/sort
	GET    /back/catalog/{objectType}/{id}/tag
	GET    /back/catalog/{objectType}/{id}/tag/{tagType}
	GET    /back/loader/{ID}
	GET    /back/loader/{ID}/edito
	GET    /back/loader/{ID}/tech
	POST   /back/preview/{objectType}
	GET    /back/ref
	GET    /back/ref/contentOffers
	GET    /back/ref/portals
	GET    /back/ref/subOffers

If you are using "Postman", you can load the file :
    "Postman - Mock Middle-Office.json.txt"
    